#include "consol_programm.h"

Consol_programm::Consol_programm()
    : start(false), stop(false), record(false)
{
    codec = QTextCodec::codecForName("UTF-8");
    connect(&udpSocket, &QUdpSocket::readyRead, this, &Consol_programm::readPendingDatagrams);
    udpSocket.bind(QHostAddress::LocalHost, 1234);
}
Consol_programm::~Consol_programm()
{
    delete gui;
}

void Consol_programm::setGUI(GUI *newGUI)
{
    gui = newGUI;
}

void Consol_programm::recordButtonClicked(const QString& N) {
    bool isInt;
    int intN = N.toInt(&isInt);


    if (start == true && stop == false){
        if (isInt){
            QVector<int> randomArray;

            for (int i = 0; i < intN; ++i) {
                randomArray.append(QRandomGenerator::global()->bounded(100));
            }

            QString arrayAsString = "{";

            for (int i = 0; i < randomArray.size(); ++i) {
                arrayAsString += QString::number(randomArray[i]);

                if (i < randomArray.size() - 1) {
                    arrayAsString += ", ";
                }
            }

            arrayAsString += "}";

            QString logMessage = "Record clicked at " + QDateTime::currentDateTime().toString() + " Recoding " + arrayAsString;
            gui->addMessage(logMessage);
            QByteArray data = logMessage.toUtf8();

            sendData(data);

        } else qDebug() << "N no int";
     }
     else qDebug() << "Нажмите кнопку старт";

}



void Consol_programm::sendData(const QByteArray &data)
{
    udpSocket.writeDatagram(data, QHostAddress::LocalHost, 1234);
}

void Consol_programm::startButtonClicked()
{
    if (start) {
        qDebug() << "Кнопка старта уже нажата";
        return;
    }
    start = true;
    stop = false;
    QByteArray data = "Start clicked at " + QDateTime::currentDateTime().toString().toUtf8();
    gui->addMessage(data);

    sendData(data);
}

void Consol_programm::stopButtonClicked()
{
    if (stop) {
        qDebug() << "Кнопка стоп уже нажата";
        return;
    }
    stop = true;
    start = false;
    QByteArray data = "Stop clicked at" + QDateTime::currentDateTime().toString().toUtf8();
    gui->addMessage(data);
    sendData(data);
}

void Consol_programm::readPendingDatagrams()
{
    while (udpSocket.hasPendingDatagrams()) {
        QByteArray datagram;
        datagram.resize(udpSocket.pendingDatagramSize());
        udpSocket.readDatagram(datagram.data(), datagram.size());
        processReceivedData(datagram);
    }
}

void Consol_programm::processReceivedData(const QByteArray &data)
{
    utf8String = codec->toUnicode(data);
    qDebug() << "Received data:" << utf8String;
}



