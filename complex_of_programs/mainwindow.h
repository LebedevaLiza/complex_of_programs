#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "QPushButton"
#include "QLineEdit"
#include "QVBoxLayout"
#include <QUdpSocket>
#include <QDateTime>
#include "gui.h"

#include "consol_programm.h"



class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
private:
    QString N;
    QPushButton *startButton;
    QPushButton *stopButton;
    QPushButton *recordButton;
    QLineEdit *lineEdit;
    QVBoxLayout *mainLayout;
    QHBoxLayout *buttonsRecordLayout;
    QHBoxLayout *buttonsStratStopLayout;
    QWidget *centralWidget;
    QUdpSocket *udpSocket;
    Consol_programm consoleProgram;
    GUI g;
signals:
    void startButtonClicked();
    void stopButtonClicked();

private slots:
    void onrecordButtonClicked();


};
#endif // MAINWINDOW_H
