#include "gui.h"

GUI::GUI(QWidget *parent)
    : QMainWindow(parent)
{
    guiListView = new QListView(this);
    guiModel = new QStringListModel(this);
    guiListView->setModel(guiModel);

    setCentralWidget(guiListView);
}

void GUI::addMessage(const QString &logMessage)
{
    QStringList logList = guiModel->stringList();
    logList.append(logMessage);
    guiModel->setStringList(logList);
}
