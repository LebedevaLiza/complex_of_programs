#ifndef CONSOL_PROGRAMM_H
#define CONSOL_PROGRAMM_H

#include <QUdpSocket>
#include <QTimer>
#include <QDateTime>
#include <QRandomGenerator>
#include <QTextCodec>
#include "QDebug"
#include <QtCore/QDataStream>
#include "gui.h"


class Consol_programm : public QObject
{
public:
    ~Consol_programm();
    Consol_programm();
    void sendData(const QByteArray &data);
    void setGUI(GUI *newGUI);

public slots:
    void startButtonClicked();
    void stopButtonClicked();
    void recordButtonClicked(const QString& N);

private slots:
    void readPendingDatagrams();
    void processReceivedData(const QByteArray &data);


private:
    QTextCodec *codec;
    QString utf8String;
    QUdpSocket udpSocket;
    bool start;
    bool stop;
    bool record;
    GUI *gui;

};

#endif // CONSOL_PROGRAMM_H
