#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    g.resize(400, 400);
    g.addMessage("Program start");
    g.show();

    consoleProgram.setGUI(&g);

    udpSocket = new QUdpSocket(this);
    udpSocket->bind(QHostAddress::LocalHost, 1234);
    lineEdit = new QLineEdit(this);
    startButton = new QPushButton("Старт", this);
    recordButton = new QPushButton("Запись", this);
    stopButton = new QPushButton("Стоп", this);
    mainLayout = new QVBoxLayout;
    buttonsRecordLayout = new QHBoxLayout;
    buttonsStratStopLayout = new QHBoxLayout;
    centralWidget = new QWidget(this);

    lineEdit->setMinimumSize(100, 30);
    lineEdit->setMaximumSize(800, 60);

    startButton->setMinimumSize(50, 20);
    startButton->setMaximumSize(100, 50);

    recordButton->setMinimumSize(50, 20);
    recordButton->setMaximumSize(100, 50);

    stopButton->setMinimumSize(50, 20);
    stopButton->setMaximumSize(100, 50);

    mainLayout->addWidget(lineEdit);
    mainLayout->addSpacing(50);
    buttonsRecordLayout->addWidget(recordButton);
    mainLayout->addLayout(buttonsRecordLayout);
    mainLayout->addSpacing(50);
    buttonsStratStopLayout->addWidget(startButton);
    buttonsStratStopLayout->addSpacing(100);
    buttonsStratStopLayout->addWidget(stopButton);
    mainLayout->addLayout(buttonsStratStopLayout);
    centralWidget->setLayout(mainLayout);
    setCentralWidget(centralWidget);



    connect(startButton, &QPushButton::clicked, &consoleProgram, &Consol_programm::startButtonClicked);
    connect(stopButton, &QPushButton::clicked, &consoleProgram, &Consol_programm::stopButtonClicked);
    connect(recordButton, &QPushButton::clicked, this, &MainWindow::onrecordButtonClicked);

    mainLayout->setAlignment(Qt::AlignHCenter);
}

MainWindow::~MainWindow()
{
}

void MainWindow::onrecordButtonClicked()
{
    N = lineEdit->text();
    consoleProgram.recordButtonClicked(N);
}
