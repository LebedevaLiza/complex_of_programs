#ifndef GUI_H
#define GUI_H


#include <QMainWindow>
#include <QListView>
#include <QStringListModel>

class GUI : public QMainWindow
{
    Q_OBJECT

public:
    GUI(QWidget *parent = nullptr);

public slots:
    void addMessage(const QString &logMessage);

private:
    QListView *guiListView;
    QStringListModel *guiModel;
};
#endif // GUI_H
